#include "crc.h"

void setup() {
        Serial.begin(115200);
}

#define DATASIZE 16

void loop() {
        uint8_t data[DATASIZE];


        for (uint8_t i = 0; i < DATASIZE; i++) {
                data[i] = i;
        }

        Serial.println();
        Serial.println(F("Data:"));
        for (uint8_t i = 0; i < DATASIZE; i++) {
                Serial.print(data[i], HEX);
                Serial.print(F(", "));
        }
        Serial.println();
        Serial.print(F("CRC8 : "));
        Serial.println(crc8block(data, DATASIZE), HEX);
        Serial.print(F("CRC16 : "));
        Serial.println(crc16block(data, DATASIZE), HEX);

        delay(2000);
}
