#ifndef CRC_H_
#define CRC_H_

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t crc8(uint8_t crc, uint8_t data);
uint8_t crc8block(uint8_t *data, uint8_t dataSize);

uint16_t crc16(uint16_t crc, uint8_t data);
uint16_t crc16block(uint8_t *data, uint8_t dataSize);

#ifdef __cplusplus
}
#endif

#endif
